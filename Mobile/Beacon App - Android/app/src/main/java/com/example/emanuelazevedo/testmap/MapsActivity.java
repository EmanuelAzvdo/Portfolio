package com.example.emanuelazevedo.testmap;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    String localActual = "";

    private static final String LOG_TAG = "MapsActivity";
    LinearLayout mapLayout;
    Button buttonBack;

    final static private int PERMISSION_REQUEST_COARSE_LOCATION = 456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.maps_activity);
        //----------------------- MAP Fragment -----------------------------------------
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent myIntent = getIntent();
        String nome = myIntent.getStringExtra("local");
        localActual = nome;

        Log.i(LOG_TAG, "TESTE: " + nome);
        // ----------------------- COMPONENTES --------------------------
        mapLayout = (LinearLayout ) findViewById(R.id.LayoutMap);
        buttonBack = (Button) findViewById(R.id.button3);

    }

    // ------------------------ GERAR MAPA --------------------------------
    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap mMap;
        mMap = googleMap;
        LatLng biblioteca;
        LatLng DAE;
        LatLng CC;
        LatLng iot;
        LatLng innolab;
        // INNOLAB - Laboratório de Inovação Em Tecnologia Da Informação (NATI)
        //LAT E LONGS
        CC = new LatLng(-3.769228, -38.479753);
        iot = new LatLng(-3.768815, -38.478522);
        innolab = new LatLng(-3.768700, -38.478849);
        biblioteca = new LatLng(-3.769064, -38.480556);
        DAE = new LatLng( -3.768990, -38.481209);
        //MARKERS
        Marker libMarker = mMap.addMarker(new MarkerOptions().position(biblioteca));
        Marker innolabMarker = mMap.addMarker(new MarkerOptions().position(innolab));
        Marker daeMarker = mMap.addMarker(new MarkerOptions().position(DAE));
        Marker ccMarker = mMap.addMarker(new MarkerOptions().position(CC));
        Marker iotMarker = mMap.addMarker(new MarkerOptions().position(iot));
        //ICONS
        libMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        innolabMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        iotMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        daeMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        ccMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        ccMarker.setAlpha(0.6f);
        ccMarker.setTitle("Centro de Convivência");
        ccMarker.setSnippet("Praça de Alimentaçao "+ '\n' +
                "Gráfica Unifor " +'\n' +
                "Loja Unifor" +'\n' +
                "CIEE" + '\n');


        if(localActual.equals("biblioteca")) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(biblioteca));
            libMarker.setTitle("BIBLIOTECA");
            libMarker.setSnippet("Você está aqui!");
            daeMarker.setTitle("DAE - Central Administrativa");
            daeMarker.setAlpha(0.6f);
            libMarker.setAlpha(1f);
            libMarker.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) 19.2));

        }
        if(localActual.equals("iot")) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(iot));
            iotMarker.setTitle("Laboratório IoT - NATI");
            iotMarker.setSnippet("Você está aqui!");
            //DAE
            daeMarker.setTitle("DAE - Central Administrativa");
            daeMarker.setAlpha(0.6f);
            //BIBLIO
            libMarker.setTitle("BIBLIOTECA");
            libMarker.setAlpha(0.6f);
            //INNOLAB
            innolabMarker.setTitle("INNOLAB - NATI");
            innolabMarker.setAlpha(0.6f);

            //SET ALPHA LAB IOT
            iotMarker.setAlpha(1f);
            iotMarker.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) 19.2));
        }
        if (localActual.equals("dae")){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(DAE));
            libMarker.setTitle("BIBLIOTECA");
            daeMarker.setTitle("DAE - Central Administrativa");
            daeMarker.setSnippet("Você esta aqui!");
            libMarker.setAlpha(0.6f);
            daeMarker.setAlpha(1f);
            daeMarker.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) 19.2));
        }
        if (localActual.equals("nenhum")){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(biblioteca));
            libMarker.setTitle("BIBLIOTECA");
            daeMarker.setTitle("DAE - Central Administrativa");
            libMarker.setAlpha(0.6f);
            daeMarker.setAlpha(0.6f);
            daeMarker.showInfoWindow();
            libMarker.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) 17.8));
        }
    }

    public void clickBack2(View view){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

}
