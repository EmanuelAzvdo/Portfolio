package com.example.emanuelazevedo.testmap;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
        import android.util.Log;
        import android.Manifest;
        import com.example.emanuelazevedo.testmap.Class.Local;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.Query;
        import android.view.View;
        import android.widget.Button;
import android.widget.LinearLayout;
        import android.widget.TextView;

        import com.google.firebase.FirebaseApp;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.example.emanuelazevedo.testmap.R.color.colorGray;
import static com.example.emanuelazevedo.testmap.R.color.colorPrimary;

public class MainActivity extends AppCompatActivity  {

    private static final String LOG_TAG = "MainActivity";
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String localActual = "nenhum";
    Button btn;
    Local p;
    LinearLayout infoLayout, mainLayout, mapLayout;
    TextView localTextView,horarioTextView,infoTextView,servicosTextView, statusTextView;

    final static private int PERMISSION_REQUEST_COARSE_LOCATION = 456 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        String uidNew = "t";
        Intent myIntent = getIntent();
        uidNew = myIntent.getStringExtra("uid");
        Log.i(LOG_TAG, "TESTE: " + uidNew);
        startService(new Intent(getBaseContext(), BluetoothService.class));


        // ----------------------- COMPONENTES --------------------------
        mapLayout = (LinearLayout ) findViewById(R.id.LayoutMap);
        infoLayout = (LinearLayout ) findViewById(R.id.LayoutInfo);
        mainLayout = (LinearLayout ) findViewById(R.id.principalLayout);
        localTextView = (TextView ) findViewById(R.id.localTextView);
        horarioTextView = (TextView ) findViewById(R.id.horarioTextView);
        infoTextView = (TextView ) findViewById(R.id.infoTextView);
        servicosTextView = (TextView ) findViewById(R.id.servicosTextView);
        statusTextView = (TextView ) findViewById(R.id.statustextView);
        btn =  findViewById(R.id.button2);
        //SET TEXT BEACON OFF
        statusTextView.setText(R.string.beaconoff);
        statusTextView.setTextColor(getResources().getColor(colorGray));
        statusTextView.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
        statusTextView.setTextSize(20);
        //-----------------------------------------------------------------
        if(uidNew!=null){
        inicializarFirebase(uidNew);
        }
      }

    private void inicializarFirebase(String uid) {

        FirebaseApp.initializeApp(MainActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        GetLocal(uid);

    }

    private void GetLocal(String uid){
        Query teste = databaseReference.child(uid);
        teste.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                p = dataSnapshot.getValue(Local.class);

                Log.i(LOG_TAG, p.getLocal());
                Log.i(LOG_TAG, p.getHorario());
                Log.i(LOG_TAG, p.getLocal());
                Log.i(LOG_TAG, p.getRecursos());
                Log.i(LOG_TAG, p.getServicos1());
                Log.i(LOG_TAG, p.getServicos2());
                Log.i(LOG_TAG, p.getServicos3());
                Log.i(LOG_TAG, p.getServicos4());
                if (p.getLocal().equals("Biblioteca")) {
                    statusTextView.setText("BIBLIOTECA");
                    localActual = "biblioteca";

                }
                if (p.getLocal().equals("DAE")) {
                        statusTextView.setText(R.string.DAE);
                        localActual = "dae";
                }
                if (p.getLocal().equals("iot")) {
                        statusTextView.setText(R.string.iot);
                        localActual = "iot";
                        statusTextView.setTextColor(getResources().getColor(colorPrimary));
                        statusTextView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                }

                String[] textoSeparado = p.getServicos1().split("@");
                String[] textoSeparado2 = p.getServicos2().split("@");
                String[] textoSeparado3 = p.getServicos3().split("@");
                // String[] textoSeparado4 = p.getServicos4().split("@");

                String Title = textoSeparado[0];
                //  String Subtile = textoSeparado[1];
                String Title2 = textoSeparado2[0];
                // String Subtile2 = textoSeparado2[1];
                String Title3 = textoSeparado3[0];
                // String Subtile3 = textoSeparado3[1];
                // String Title4 = textoSeparado4[0];
                // String Subtile4 = textoSeparado4[1];
                String servicos = "1. " + Title + '\n' +
                        "2." + Title2 + '\n' +
                        "3." + Title3 + '\n';

                p.setServicos(servicos);
                infoTextView.setText(p.getRecursos());
                localTextView.setText(R.string.iot);
                horarioTextView.setText(p.getHorario());
                servicosTextView.setText(servicos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    // ----------------------- EVENTOS --------------------------
    public void clickBtn2(View view){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("local", localActual);
        startActivity(intent);
    }
    public void clickBtn1(View view){
        mainLayout.setVisibility(View.INVISIBLE);
        infoLayout.setVisibility(View.VISIBLE);

    }
    public void clickBack(View view){
        mainLayout.setVisibility(View.VISIBLE);
        infoLayout.setVisibility(View.INVISIBLE);

    }
    // ------------------------------------------------------------
}
