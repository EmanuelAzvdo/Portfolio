package com.example.emanuelazevedo.testmap.Class;


public class Local {

    private String Horario;
    private String Local;
    private String recursos;
    private String Servicos1;
    private String Servicos2;
    private String Servicos3;
    private String Servicos4;

    public String getServicos() {
        return Servicos;
    }

    public void setServicos(String servicos) {
        Servicos = servicos;
    }

    private String Servicos;


    public Local() {

    }

    public String getHorario() {
        return Horario;
    }

    public void setHorario(String horario){
        Horario = horario;
    }

    public String getLocal() {
        return Local;
    }

    public void setLocal(String local) {
        Local = local;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    public String getServicos1() {
        return Servicos1;
    }

    public void setServicos1(String servicos1) {
        Servicos1 = servicos1;
    }

    public String getServicos2() {
        return Servicos2;
    }

    public void setServicos2(String servicos2) {
        Servicos2 = servicos2;
    }

    public String getServicos3() {
        return Servicos3;
    }

    public void setServicos3(String servicos3) {
        Servicos3 = servicos3;
    }

    public String getServicos4() {
        return Servicos4;
    }

    public void setServicos4(String servicos4) {
        Servicos4 = servicos4;
    }



}
