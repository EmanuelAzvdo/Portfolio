import { ContactProvider } from './../../providers/contact/contact';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  co: Observable<any>;
  glp: Observable<any>;
  lamp: Observable<any>;
  temp: Observable<any>;
  umid: Observable<any>;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private provider: ContactProvider) {

    this.co = this.provider.getCo();
    this.co.forEach(item => {
    console.log('CO:', item[0].val);
    });
    this.glp = this.provider.getGlp();
    this.glp.forEach(item => {
    console.log('GLP:', item[0].val);
    });
    this.lamp = this.provider.getLamp();
    this.lamp.forEach(item => {
    console.log('Lamp:', item[0].val);
    });
    this.temp = this.provider.getTemp();
    this.temp.forEach(item => {
    console.log('Temp:', item[0].val);
    });
    this.umid = this.provider.getUmid();
    this.umid.forEach(item => {
    console.log('Umid:', item[0].val);
    });
  }

  Loading() {
    let loader = this.loadingCtrl.create({
      content: "Carregando...",
      spinner: 'bubbles'
    });
    loader.present();
    setTimeout(() => {
    loader.dismiss();
    this.showAlert();
  }, 1000);

  }
  showAlert(){
  let alert = this.alertCtrl.create({
    title: 'Aviso',
    subTitle: 'Temperatura está alta, favor refrigerar ambiente para sua segurança.',
    buttons: ['OK']
  });
  alert.present();
}

}
