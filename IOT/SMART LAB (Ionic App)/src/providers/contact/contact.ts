import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ContactProvider {
  private PATH = '/co';
  private PATH2 = '/glp';
  private PATH3 = '/lamp';
  private PATH4 = '/temp';
  private PATH5 = '/umid'

  constructor(private db: AngularFireDatabase) {
}
  getCo() {
    return this.db.list(this.PATH)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, val: c.payload.val() }));
      })
  }
  getGlp() {
    return this.db.list(this.PATH2)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, val: c.payload.val() }));
      })
  }
  getLamp() {
    return this.db.list(this.PATH3)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, val: c.payload.val() }));
      })
  }
  getTemp() {
    return this.db.list(this.PATH4)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, val: c.payload.val() }));
      })
  }
  getUmid() {
    return this.db.list(this.PATH5)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, val: c.payload.val() }));
      })
  }

}
