(function  () {
  angular.module('grupoShark')
    .factory('HttpService', HttpService);

  HttpService.$inject = ['$http'];

  function HttpService ($http) {
    var api = "http://localhost:3000/api/clientes";
    //var api2 = "//35.229.111.4:8076/REST/PRODUTOSREST?CODPRODUTO=";

    function getProdutos() {
      return $http.get(api)
      .then(function(response) {
        return response;
      });
   }

  	return {
     getProdutos: getProdutos
  	}
  }

})();
