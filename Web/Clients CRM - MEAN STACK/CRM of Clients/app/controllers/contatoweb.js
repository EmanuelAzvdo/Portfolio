//----------FUNCTIONS MONGO DB--------------------------------


module.exports = function(app) {
  var ContatoModelWeb = app.models.contatoweb;
  var controllerweb = {}

  //FUNÇõES------------------------------------------
  controllerweb.salvaContato = function(req, res) {
    var _id = req.body._id;
    if (_id) {
      ContatoModelWeb.findByIdAndUpdate(_id, req.body).exec()
        .then(
          function(contato) {
            res.json(contato);
          },
          function(erro) {
            console.error(erro)
            res.status(500).json(erro);
          }
        );
    } else {
      ContatoModelWeb.create(req.body)
        .then(
          function(contato) {
            res.status(201).json(contato);
          },
          function(erro) {
            console.log(erro);
            res.status(500).json(erro);
          });
    }
  };

  //-------------------------------------------------
  controllerweb.removeContato = function(req, res) {
    var _id = req.params.id;
    ContatoModelWeb.remove({
        "_id": _id
      }).exec()
      .then(
        function() {
          res.end();
        },
        function(erro) {
          return console.error(erro);
        }
      );

  };
  //------------------------------------------------
  controllerweb.obtemContato = function(req, res) {
    var _id = req.params.id;
    ContatoModelWeb.findById(_id).exec()
      .then(function(contato) {
          if (!contato) throw new Error("Contato não encontrado");
          res.json(contato)
        },
        function(erro) {
          console.log(erro);
          res.status(404).json(erro)
        });
  };

  controllerweb.listaContatos = function(req, res) {
    ContatoModelWeb.find()
      .exec()
      .then(
        function(contatos) {
          for (var i = 0; i < contatos.length; i++) {

            var date1 = new Date(contatos[i].datafim);
            var date2 = new Date();
            var diferenca = Math.abs(date1 - date2); //diferença em milésimos e positivo
            var dia = 1000 * 60 * 60 * 24; // milésimos de segundo correspondente a um dia
            var total = Math.round(diferenca / dia); //valor total de dias arredondado

            var meses = (total / 30);
            var resto = meses % 1;
            var dias = resto * 30;
            var mesesInt = Math.trunc(meses);
            var diasInt = Math.trunc(dias);
            if (mesesInt < 3) {
              contatos[i].alert = true;
            } else {
              contatos[i].alert = false;
            }

            console.log(date1)
            console.log(date2)
            console.log(total + " dias")
            console.log(meses + " meses e " + dias + " dias")
            console.log(mesesInt + " meses e " + diasInt + " dias")
            if (mesesInt == 0) {
              if (diasInt == 1) {
                contatos[i].contrato = diasInt + ' dia';
              } else {
                contatos[i].contrato = diasInt + ' dias';
              }
            } else {
              if (resto == 0) {
                if (mesesInt == 1) {
                  contatos[i].contrato = mesesInt + ' mês';
                } else {
                  contatos[i].contrato = mesesInt + ' meses';
                }
              } else {
                if (contatos[i].datafim) {
                  if (mesesInt == 1 && diasInt == 1)
                    contatos[i].contrato = mesesInt + ' mês e ' + diasInt + ' dia';
                  if (mesesInt == 1 && diasInt > 1)
                    contatos[i].contrato = mesesInt + ' mês e ' + diasInt + ' dias';
                  if (mesesInt > 1 && diasInt == 1)
                    contatos[i].contrato = mesesInt + ' meses e ' + diasInt + ' dia';
                  if (mesesInt > 1 && diasInt > 1)
                    contatos[i].contrato = mesesInt + ' meses e ' + diasInt + ' dias';
                }
              }
            }
          }
          res.json(contatos);
        },
        function(erro) {
          console.error(erro)
          res.status(500).json(erro);
        });
  };
  return controllerweb;
}

//-----------------------------------------------
