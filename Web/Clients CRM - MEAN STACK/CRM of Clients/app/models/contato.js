var mongoose = require('mongoose');
module.exports = function() {
  var schema = mongoose.Schema({
      nome: {
        type: String,
        required: true
      },
      email: {
        type: String,
        required: true,
      },
      empresa: {
        type: String,
        required: true
      },
      endereco: {
        type: String,
      },
      cnpj: {
        type: String,
      },
      telefone: {
        type: String,
      },
      observacoes: {
        type: String,
      },
      datafim:{
        type: String,
      },
      datainicio:{
        type: String,
      },
      contrato:{
        type: String,
      },
      alert:{
        type: Boolean,
      }
  });
  return mongoose.model('contatos', schema);
};
