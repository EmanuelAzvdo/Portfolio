
module.exports = function(app) {
  var controllerweb = app.controllers.contatoweb;

  app.route('/contatosweb')
    .get(controllerweb.listaContatos)
    .post(controllerweb.salvaContato);
  app.route('/contatosweb/:id')
    .get(controllerweb.obtemContato)
    .delete(controllerweb.removeContato);

};
