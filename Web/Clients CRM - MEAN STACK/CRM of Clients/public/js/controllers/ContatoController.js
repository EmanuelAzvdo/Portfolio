angular.module('contatooh').controller('ContatoController',
  function($scope, $routeParams, $resource) {

    var Contato = $resource('/contatos/:id');

    // ------------------- POST Contato Editado --------------------------
    $scope.addeditarContato = function() {
      $scope.contato.$save()
        .then(function() {
          $scope.mensagem = {
            texto: 'Salvo com sucesso'
          };
          // limpa o formulário
          $scope.contato = new Contato();
        })
        .catch(function(erro) {
          $scope.mensagem = {
            texto: 'Não foi possível salvar'
          };
        });
    }
    $scope.MascaraTelefone = function(tel){

        if(mascaraInteiro(tel)==false){
                event.returnValue = false;
        }
        return formataCampo(tel, '(00) 0000-0000', event);
}

$scope.ValidaTelefone = function(tel){
    exp = /\(\d{2}\)\ \d{4}\-\d{4}/
    if(!exp.test(tel.value))
            alert('Numero de Telefone Invalido!');
}
    //-------------------------------------------------------------------
    // ------------------ BUSCA CONTATO por ID da url --------
    if ($routeParams.contatoId) {
      Contato.get({
          id: $routeParams.contatoId
        },
        function(contato) {
          $scope.contato = contato;
        },
        function(erro) {
          $scope.mensagem = {
            texto: 'Contato não existe. Novo contato.'
          };
        });
    } else {
      $scope.contato = new Contato();
    }

    //-------------------------------------------------------------
    // --------- DATEPICKER FUNÇÔES ----------------------------

    jQuery(function() {

      jQuery('#datetimepicker_dark').datetimepicker({
        timepicker: false,
        inline: false,
        theme: 'dark',
        format: 'Y/m/d'
      });
      jQuery('#datetimepicker_dark2').datetimepicker({
        timepicker: false,
        inline: false,
        theme: 'dark',
        format: 'Y/m/d'
      });
    });

  });
