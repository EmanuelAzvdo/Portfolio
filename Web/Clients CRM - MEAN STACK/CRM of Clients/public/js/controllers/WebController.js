angular.module('contatooh').controller('WebController',
  function($scope, $routeParams, $resource) {

    var Contatoweb = $resource('/contatosweb/:id');
    

    // ------------------- POST Contato Editado --------------------------
    $scope.addeditarContato = function() {
      $scope.contato.$save()
        .then(function() {
          $scope.mensagem = {
            texto: 'Salvo com sucesso'
          };
          // limpa o formulário
          $scope.contato = new Contatoweb();
        })
        .catch(function(erro) {
          $scope.mensagem = {
            texto: 'Não foi possível salvar'
          };
        });
    }
    //-------------------------------------------------------------------
    // ------------------ BUSCA CONTATO por ID da url --------
    if ($routeParams.contatoId) {
      Contatoweb.get({
          id: $routeParams.contatoId
        },
        function(contato) {
          $scope.contato = contato;
        },
        function(erro) {
          $scope.mensagem = {
            texto: 'Contato não existe. Novo contato.'
          };
        });
    } else {
      $scope.contato = new Contatoweb();
    }

    //-------------------------------------------------------------
    // --------- DATEPICKER FUNÇÔES ----------------------------

    jQuery(function() {

      jQuery('#datetimepicker_darkweb').datetimepicker({
        timepicker: false,
        inline: false,
        theme: 'dark',
        format: 'Y/m/d'
      });
      jQuery('#datetimepicker_darkweb2').datetimepicker({
        timepicker: false,
        inline: false,
        theme: 'dark',
        format: 'Y/m/d'
      });
    });

  });
