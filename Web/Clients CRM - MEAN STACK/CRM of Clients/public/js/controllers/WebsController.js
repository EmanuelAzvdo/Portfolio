angular.module('contatooh').controller('WebsController',
  function($scope, $resource) {

    var Contato = $resource('/contatosweb/:id');
    var ContatoId;
    $scope.mensagem = {
      texto: ''
    };
    $scope.filtro = '';
    $scope.contatos = [];
    $scope.init = function() {
      buscaContatos();
    }

    //---- CARREGA CONTATOS do Express p/ SCOPO -----------------
    function buscaContatos() {
      Contato.query(
        function(contatos) {
          $scope.contatos = contatos;
        },
        function(erro) {
          $scope.mensagem = {
            texto: "Não foi possível obter a lista!"
          };
        });

    }
    $scope.guardar = function(contato) {
      console.log(contato)
      ContatoId = contato;
    }

    $scope.remover = function() {

      remove(ContatoId);
    }
    //---- BUTTON REMOVER CONTATO ------------
    remove = function(contato) {
      console.log(contato)
      Contato.delete({
          id: contato._id
        },
        buscaContatos,
        function(erro) {
          $scope.mensagem = {
            texto: "Não foi possível remover o contato!"
          };
          console.log(erro);
        });
      $('.modal-backdrop').hide()
    }

    //------------------------------------------------

    $scope.init();
  });
