angular.module('contatooh', ['ngRoute', 'ngResource'])
  .config(function($routeProvider) {

    $routeProvider.when('/', {
      templateUrl: 'partials/inicial.html',

    });
    $routeProvider.when('/english', {
      templateUrl: 'partials/contatos.html',
      controller: 'ContatosController'
    });
    $routeProvider.when('/web', {
      templateUrl: 'partials/Webs.html',
      controller: 'WebsController'
    });
    $routeProvider.when('/englishCadastro', {
      templateUrl: 'partials/contato.html',
      controller: 'ContatoController'
    });
    $routeProvider.when('/webCadastro', {
      templateUrl: 'partials/Web.html',
      controller: 'WebController'
    });
    $routeProvider.when('/englishCadastro/:contatoId', {
      templateUrl: 'partials/contato.html',
      controller: 'ContatoController'
    });
    $routeProvider.when('/webCadastro/:contatoId', {
      templateUrl: 'partials/Web.html',
      controller: 'WebController'
    });


  });
