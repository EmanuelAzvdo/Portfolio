var load = require('express-load');
var bodyParser = require('body-parser');
var express = require('express');

//var home = require('../app/routes/home');


module.exports = function() {
  var app = express();

  // configuração de ambiente
  app.set('port', 3000);
  app.set('view engine', 'ejs');
  app.set('views', './app/views');

  app.use(express.static('./public'));
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(require('method-override')())


  load('models', {
      cwd: 'app'
    })
    .then('controllers')
    .then('routes')
    .into(app);

  return app;

};
