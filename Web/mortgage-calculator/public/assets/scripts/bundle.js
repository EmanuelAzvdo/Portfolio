/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/main.js":
/*!*****************************!*\
  !*** ./src/scripts/main.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./src/scripts/utils.js\");\n\nvar marjorityFields = ['js--input-loanAmount', 'js--input-tax', 'js--input-insurance'];\nvar errorClassMsgs = ['message-amount', 'message-tax', 'message-insurance'];\nvar formButton = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])('js--submit');\nvar yearsRange = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])('js--years-range');\nvar interestRange = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])('js--interest-range');\n\nformButton.onclick = function () {\n  validateForm();\n};\n\nyearsRange.addEventListener(\"input\", function () {\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"updateRangeValue\"])('js--input-years', yearsRange, yearsRange.getAttribute('max'));\n}, false);\ninterestRange.addEventListener(\"input\", function () {\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"updateRangeValue\"])('js--input-interest', interestRange, interestRange.getAttribute('max'));\n}, false);\n\nvar validateForm = function validateForm() {\n  var validResults = marjorityFields.map(function (el) {\n    return Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"validateField\"])(el);\n  });\n  handleErros(validResults);\n\n  if (!validResults.includes(false)) {\n    calcResults();\n  }\n\n  ;\n};\n\nvar handleErros = function handleErros(results) {\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"checkScreen\"])(errorClassMsgs);\n  results.forEach(function (element, index) {\n    element ? Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"removeClass\"])(errorClassMsgs[index], 'show') : Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"addClass\"])(errorClassMsgs[index], 'show');\n  });\n};\n\nvar calcResults = function calcResults() {\n  var amount = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])(marjorityFields[0]);\n  var tax = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])(marjorityFields[1]);\n  var insurance = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"getElement\"])(marjorityFields[2]);\n  var annualTax = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"annualTax_Insurance\"])(tax.value);\n  var interest = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"PrincipalInterest\"])(interestRange.value, amount.value, yearsRange.value);\n  var annualInsurence = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"annualTax_Insurance\"])(insurance.value);\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"setText\"])('js--result-tax', roundValue(Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"annualTax_Insurance\"])(tax.value)));\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"setText\"])('js--result-insurance', roundValue(Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"annualTax_Insurance\"])(insurance.value)));\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"setText\"])('js--result-principleAndInterest', roundValue(Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"PrincipalInterest\"])(interestRange.value, amount.value, yearsRange.value)));\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"setText\"])('js--result-total', roundValue(interest + annualInsurence + annualTax));\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"addAllClass\"])('ResultsBox__response', 'calculated');\n  Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"addClass\"])('ResultsBox', 'mobileAnimation');\n  setTimeout(function () {\n    window.scrollBy({\n      top: window.innerHeight,\n      left: 0,\n      behavior: 'smooth'\n    });\n  }, 650);\n};\n\nvar roundValue = function roundValue(num) {\n  return Math.round(num * 100) / 100;\n};\n\n//# sourceURL=webpack:///./src/scripts/main.js?");

/***/ }),

/***/ "./src/scripts/utils.js":
/*!******************************!*\
  !*** ./src/scripts/utils.js ***!
  \******************************/
/*! exports provided: PrincipalInterest, annualTax_Insurance, monthlyPayment, getElement, addClass, addAllClass, removeClass, setText, validateField, updateRangeValue, checkScreen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PrincipalInterest\", function() { return PrincipalInterest; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"annualTax_Insurance\", function() { return annualTax_Insurance; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"monthlyPayment\", function() { return monthlyPayment; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getElement\", function() { return getElement; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addClass\", function() { return addClass; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addAllClass\", function() { return addAllClass; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"removeClass\", function() { return removeClass; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setText\", function() { return setText; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"validateField\", function() { return validateField; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"updateRangeValue\", function() { return updateRangeValue; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"checkScreen\", function() { return checkScreen; });\n// FORMULAS\nvar PrincipalInterest = function PrincipalInterest(interestRate, loanAmount, yearsOfMortgage) {\n  return interestRate / 100 / 12 * loanAmount / (1 - Math.pow(1 + interestRate / 100 / 12, -yearsOfMortgage * 12));\n};\nvar annualTax_Insurance = function annualTax_Insurance(_annualTax_Insurance) {\n  return _annualTax_Insurance / 12;\n};\nvar monthlyPayment = function monthlyPayment(aprincipleAndInterests, Tax, Insurance) {\n  return aprincipleAndInterests + Tax + Insurance;\n}; // MANAGE CLASSES AND VALUES\n\nvar getElement = function getElement(className) {\n  return document.getElementsByClassName(className)[0];\n};\nvar addClass = function addClass(item, className) {\n  getElement(item).classList.add(className);\n};\nvar addAllClass = function addAllClass(item, className) {\n  var elements = document.getElementsByClassName(item);\n  var _iteratorNormalCompletion = true;\n  var _didIteratorError = false;\n  var _iteratorError = undefined;\n\n  try {\n    for (var _iterator = elements[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {\n      var value = _step.value;\n      value.classList.add(className);\n    }\n  } catch (err) {\n    _didIteratorError = true;\n    _iteratorError = err;\n  } finally {\n    try {\n      if (!_iteratorNormalCompletion && _iterator.return != null) {\n        _iterator.return();\n      }\n    } finally {\n      if (_didIteratorError) {\n        throw _iteratorError;\n      }\n    }\n  }\n};\nvar removeClass = function removeClass(item, className) {\n  getElement(item).classList.remove(className);\n};\nvar setText = function setText(item, text) {\n  getElement(item).innerHTML = text;\n};\nvar validateField = function validateField(item) {\n  var _getElement = getElement(item),\n      value = _getElement.value;\n\n  if (value) {\n    removeClass(item, 'error');\n    return true;\n  } else {\n    addClass(item, 'error');\n    return false;\n  }\n};\nvar updateRangeValue = function updateRangeValue(item, rangeRef, maxValue) {\n  getElement(item).value = rangeRef.value;\n  rangeRef.style.backgroundSize = \"\".concat(rangeRef.value / maxValue * 100, \"% 100%\");\n};\nvar checkScreen = function checkScreen(errorClassMsgs) {\n  if (window.innerWidth < 430) {\n    errorClassMsgs.forEach(function (element) {\n      setText(element, 'Mandatory field');\n    });\n  }\n};\n\n//# sourceURL=webpack:///./src/scripts/utils.js?");

/***/ }),

/***/ "./src/styles/main.scss":
/*!******************************!*\
  !*** ./src/styles/main.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/styles/main.scss?");

/***/ }),

/***/ 0:
/*!**********************************************************!*\
  !*** multi ./src/scripts/main.js ./src/styles/main.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./src/scripts/main.js */\"./src/scripts/main.js\");\nmodule.exports = __webpack_require__(/*! ./src/styles/main.scss */\"./src/styles/main.scss\");\n\n\n//# sourceURL=webpack:///multi_./src/scripts/main.js_./src/styles/main.scss?");

/***/ })

/******/ });