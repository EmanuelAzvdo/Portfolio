
(function () {
  let titles = 'Mortgage Calculator';
  let styleTemplates = `<style>@import './assets/styles/main.css'</style>`;
  let template = `<div class="MainBody">
    <header class="MainHeader">
      <h1 class="MainHeader__title"></h1>
      <div class="MainHeader__divider"></div>
    </header>
    <main>
      <div class="Calculator Calculator--box">
        <h1 class="Calculator__title">Change values to calculate your results</h1>
        <div class="Calculator__divider"></div>

        <form class="Calculator__form">
          <h4 class="form__title">Years of mortgate</h4>
          <div class="range__box">
            <p class="first-measure">1</p>
            <input class="form__range" type="range" name="" min="1" max="40">
            <p>40</p>
            <input class="form__input__range" type="number" name="yearsOfMortgage" readonly>
          </div>
          <h4 class="form__title">Rate of interest (%)</h4>
          <div class="range__box">
            <p>0.1</p>
            <input class="form__range" type="range" name="" min="0.1" max="10">
            <p>10</p>
            <input class="form__input__range" type="number" step="any" name="interestRate" readonly>
          </div>

          <h4 class="form__title">Loan Amount</h4>
          <div class="form__input__box">
            <input value="" autofocus="autofocus" type="number" name="loanAmount" autocomplete="off" />
            <span class="unit">$</span>
          </div>
          <div class="form_inline_fields">
            <div class="form__input__box">
              <h4 class="form__title">Annual Tax</h4>
              <input value="" autofocus="autofocus" type="number" name="loanAmount" autocomplete="off" />
              <span class="unit">$</span>
            </div>
            <div class="form__input__box">
              <h4 class="form__title">Annual Insurance</h4>
              <input value="" autofocus="autofocus" type="number" name="loanAmount" autocomplete="off" />
              <span class="unit">$</span>
            </div>
          </div>

          <button class="form__button js--submit" type="button">CALCULATE</button>
        </form>
      </div>
      <div class="ResultsBox">
        <h1>Your Results</h1>
        <br>
        <h4>Principle & Interest</h4>
        <p class="ResultsBox__response calculated">$ <span class="js--result-principleAndInterest">--</span></p>
        <h4>Tax</h4>
        <p class="ResultsBox__response calculated">$ <span class="js--result-tax">--</span></p>
        <h4>Insurance</h4>
        <p class="ResultsBox__response calculated">$ <span class="js--result-insurance">--</span></p>
      </div>
    </main>
  </div>
  ${styleTemplates}`;

  class MortageCalculator extends HTMLElement {
    constructor() {
      let ModalOpenBtn = document.getElementsByClassName('js--submit')[0];
      console.log(ModalOpenBtn);
      ModalOpenBtn.onclick = () => {
        console.log('clickkk-shasdasdasdadadow');
      }
    }
    // Fires when an instance of the element is created.
    createdCallback() {
      this.createShadowRoot().innerHTML = template;
      const formButton = this.shadowRoot.querySelector('.js--submit');
      let ModalOpenBtn = document.getElementsByClassName('js--submit')[0];
      console.log(ModalOpenBtn);
      console.log(formButton);
      formButton.onclick = () => {
        console.log('clickkk-shadow');
      }
      this.draw();
    };
 
    draw() { };
    
  }
  document.registerElement('mortgage-calculator', MortageCalculator);
})();