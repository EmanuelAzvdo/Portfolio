// FORMULAS
export const PrincipalInterest = (interestRate, loanAmount, yearsOfMortgage) => {
    return ((interestRate / 100) / 12) * loanAmount / (1 - Math.pow((1 + ((interestRate / 100) / 12)), - yearsOfMortgage * 12));
};

export const annualTax_Insurance = (annualTax_Insurance) => {
    return (annualTax_Insurance / 12);
}

export const monthlyPayment = (aprincipleAndInterests, Tax, Insurance) => {
    return (aprincipleAndInterests + Tax + Insurance);
}

// MANAGE CLASSES AND VALUES
export const getElement = (className) => {
    return  document.getElementsByClassName(className)[0];
}

export const addClass = (item, className) => {
    getElement(item).classList.add(className);
}

export const addAllClass = (item, className) => {
    const elements = document.getElementsByClassName(item);

    for (var value of elements) {
        value.classList.add(className);
    }
}

export const removeClass = (item, className) => {
    getElement(item).classList.remove(className);
}

export const setText = (item, text) => {
    getElement(item).innerHTML = text;
}

export const validateField = (item) => {
    let { value } = getElement(item);
    if (value) {
        removeClass(item, 'error');
        return true;
    } else {
        addClass(item, 'error');
        return false;
    }
}

export const updateRangeValue = (item, rangeRef, maxValue) => {
    getElement(item).value = rangeRef.value;
    rangeRef.style.backgroundSize = `${(rangeRef.value / maxValue) * 100}% 100%`;
}

export const checkScreen = (errorClassMsgs) => {
    if (window.innerWidth < 430) {
        errorClassMsgs.forEach(element => { setText(element, 'Mandatory field') });
    }
}



