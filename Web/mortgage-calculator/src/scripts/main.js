import { PrincipalInterest, annualTax_Insurance, monthlyPayment,
     updateRangeValue, validateField, getElement, addClass, addAllClass, removeClass, setText, checkScreen } from './utils';

const marjorityFields = ['js--input-loanAmount', 'js--input-tax', 'js--input-insurance'];
const errorClassMsgs = ['message-amount', 'message-tax', 'message-insurance'];
const formButton = getElement('js--submit');
const yearsRange = getElement('js--years-range');
const interestRange = getElement('js--interest-range');

formButton.onclick = () => {
    validateForm();
}

yearsRange.addEventListener("input", function () {
    updateRangeValue('js--input-years', yearsRange, yearsRange.getAttribute('max'));
}, false);

interestRange.addEventListener("input", function () {
    updateRangeValue('js--input-interest', interestRange, interestRange.getAttribute('max'));
}, false);

const validateForm = () => {
    let validResults = marjorityFields.map((el => validateField(el)));
    handleErros(validResults);
    if(!validResults.includes(false)) { calcResults() };
}
const handleErros = (results) => {
    checkScreen(errorClassMsgs);
    results.forEach((element, index) => {
        element ? removeClass(errorClassMsgs[index], 'show') : addClass(errorClassMsgs[index], 'show');
    });
}

const calcResults = () => {
    const amount = getElement(marjorityFields[0]);
    const tax = getElement(marjorityFields[1]);
    const insurance = getElement(marjorityFields[2]);
    const annualTax = annualTax_Insurance(tax.value);
    const interest = PrincipalInterest(interestRange.value, amount.value, yearsRange.value);
    const annualInsurence = annualTax_Insurance(insurance.value);
    
    setText('js--result-tax', roundValue(annualTax_Insurance(tax.value)));
    setText('js--result-insurance', roundValue(annualTax_Insurance(insurance.value)));
    setText('js--result-principleAndInterest', roundValue(PrincipalInterest(interestRange.value, amount.value, yearsRange.value)));
    setText('js--result-total', roundValue(interest + annualInsurence + annualTax));
    addAllClass('ResultsBox__response', 'calculated');
    addClass('ResultsBox', 'mobileAnimation');
    setTimeout(function(){
        window.scrollBy({ 
        top: window.innerHeight, 
        left: 0,
        behavior: 'smooth' 
      })}, 650);
}

const roundValue = (num) => {
    return Math.round(num * 100) / 100
}